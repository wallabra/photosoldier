#ifndef GEOMETRY_HEADER
#define GEOMETRY_HEADER

#include "commons.h"



typedef struct SphereData_s {
    Vec3_t pos;
    float rad;
} SphereData_t;

typedef struct BoxData_s {
    Vec3_t pos;
    Vec3_t sizes;
} BoxData_t;


float G_dist_sphere(SceneObject_t* me, Vec3_t* to);
float G_dist_box(SceneObject_t* me, Vec3_t* to);

void G_init_sphere(SceneObject_t* me, Vec3_t* pos, float rad);
void G_init_box(SceneObject_t* me, Vec3_t* pos, Vec3_t* box);


#endif