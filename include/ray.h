#ifndef RAY_HEADER
#define RAY_HEADER

#include "commons.h"



Ray_t* r_emptyNew();
void r_advance(Ray_t* ray, float dist);
Ray_t* r_fromTo(Vec3_t* begin, Vec3_t* end);
Ray_t* r_fromToward(Vec3_t* begin, Vec3_t* toward);
void r_done(Ray_t* ray);
Vec3_t* r_currentPos(Ray_t* ray);
float r_sceneDistance(Ray_t* ray, Scene_t* scene);



#endif