#ifndef COMMONS_HEADER
#define COMMONS_HEADER

#include <stdlib.h>



// Basic mathematical constructs
typedef struct Vec2_s {
    float x, y;
} Vec2_t;

typedef struct Vec3_s {
    float x, y, z;
} Vec3_t;
typedef Vec3_t Color_t;

typedef struct Rot_s {
    float p, y, r; // pitch, yaw, roll
} Rot_t;

struct Scene_s;


// Geometrical constructs
typedef struct Material_s {
    Color_t emission, diffuse;
    unsigned short reflectivity; // reflectivity = 0, pure diffuse color; reflectivity = 65535, pure specular (mirror reflection)
} Material_t;


extern unsigned long lastID;

typedef struct SceneObject_s {
    unsigned long id;
    void* data;                                                         // data assigned to this object (position, size, etc.)

    Material_t material;                                                // the material of this object
    
    float (*distanceFunc)(struct SceneObject_s* me, Vec3_t* to);        // the distance function used to ray march

    struct SceneObject_s* prev;
    struct SceneObject_s* next;
    struct Scene_s* scene;
} SceneObject_t;

typedef float (*DistanceFunc_t)(SceneObject_t*, Vec3_t*);


typedef struct Camera_s {
    Vec3_t origin;
    float fov;
    Rot_t toward; // direction rotation
} Camera_t;


typedef struct Ray_s {
    Vec3_t* origin;
    Camera_t* from;
    Vec3_t* toward; // MUST be normalized when set
    float currDist;
    SceneObject_t* closest;
} Ray_t;


typedef struct Scene_s {
    float nearDist, farDist;
    Color_t fogColor;

    struct SceneObject_s* objects;
} Scene_t;

// Constructing images
typedef struct Bitmap_s {
    Color_t *pixels;
    size_t width;
    size_t height;
} Bitmap_t;



#endif // COMMONS_HEADER