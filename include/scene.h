#ifndef SCENE_HEADER
#define SCENE_HEADER

#include "commons.h"



SceneObject_t* s_addObject(Scene_t* scene, DistanceFunc_t distanceFunc, void* data, Material_t* material);
void s_objDone(SceneObject_t* obj);
void s_allDone(Scene_t* scene, unsigned char freeScene);



#endif