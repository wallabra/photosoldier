#include "commons.h"
#include "scene.h"



unsigned long lastID = 0;


SceneObject_t* s_addObject(Scene_t* scene, DistanceFunc_t distanceFunc, void* data, Material_t* material) {
    SceneObject_t* res = scene->objects;
    SceneObject_t* prev = 0;

    if (res == 0) {
        scene->objects = malloc(sizeof(SceneObject_t));
        res = scene->objects;
    }

    else {
        while (res->next != 0) res = res->next;

        prev = res;
        res = res->next = malloc(sizeof(SceneObject_t));
    }

    res->distanceFunc = distanceFunc;
    res->data = data;
    res->material = *material;
    res->prev = prev;
    res->id = lastID++;

    return res;
}

void s_objDone(SceneObject_t* obj) {
    if (obj->scene->objects == obj) obj->scene->objects = obj->next;
    if (obj->prev) obj->prev->next = obj->next;
    if (obj->next) obj->next->prev = obj->prev;
    
    free(obj);
}

void s_allDone(Scene_t* scene, unsigned char freeScene) {
    SceneObject_t* cur = scene->objects;

    while (cur && cur->next) {
        cur = cur->next;
        free(cur->prev);
    }

    if (cur) free(cur);

    if (freeScene) free(scene);
}