#include <stdio.h>
#include <png.h>

#include "commons.h"
#include "bitmap.h"
#include "marcher.h"
#include "camera.h"
#include "vector3.h"



// Credits: https://www.lemoda.net/c/write-png/
int b_writePNG(Bitmap_t *bitmap, FILE *fp)
{
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    size_t x, y;
    png_byte **row_pointers = NULL;

    /* "status" contains the return value of this function. At first
       it is set to a value which means 'failure'. When the routine
       has finished its work, it is set to a value which means
       'success'. */
    int status = -1;

    /* The following number is set by trial and error only. I cannot
       see where it it is documented in the libpng manual.
    */
    int pixel_size = 3;
    int depth = 8;

    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (png_ptr == NULL)
    {
        goto png_create_write_struct_failed;
    }

    info_ptr = png_create_info_struct(png_ptr);

    if (info_ptr == NULL)
    {
        goto png_create_info_struct_failed;
    }

    /* Set up error handling. */

    if (setjmp(png_jmpbuf(png_ptr)))
    {
        goto png_failure;
    }

    /* Set image attributes. */

    png_set_IHDR(
        png_ptr,
        info_ptr,
        bitmap->width,
        bitmap->height,
        depth,

        PNG_COLOR_TYPE_RGB,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT
    );

    /* Initialize rows of PNG. */

    row_pointers = png_malloc(png_ptr, bitmap->height * sizeof(png_byte *));

    for (y = 0; y < bitmap->height; y++)
    {
        png_byte *row = png_malloc(png_ptr, sizeof(unsigned char) * bitmap->width * pixel_size);
        row_pointers[y] = row;

        for (x = 0; x < bitmap->width; x++)
        {
            Color_t *pixel = b_at(bitmap, x, bitmap->height - y);

            *row++ = (png_byte) (pixel->x * 255);
            *row++ = (png_byte) (pixel->y * 255);
            *row++ = (png_byte) (pixel->z * 255);
        }
    }

    /* Write the image data to "fp". */

    png_init_io(png_ptr, fp);
    png_set_rows(png_ptr, info_ptr, row_pointers);
    png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    /* The routine has successfully written the file, so we set
       "status" to a value which indicates success. */

    for (y = 0; y < bitmap->height; y++)
    {
        png_free(png_ptr, row_pointers[y]);
    }

    png_free(png_ptr, row_pointers);

    status = 0;

png_failure:
png_create_info_struct_failed:
    png_destroy_write_struct(&png_ptr, &info_ptr);

png_create_write_struct_failed:
    fclose(fp);

    return status;
}

Color_t *b_at(Bitmap_t *bitmap, size_t x, size_t y)
{
    return bitmap->pixels + bitmap->width * y + x;
}

Bitmap_t *b_alloc(size_t width, size_t height)
{
    Bitmap_t *bitmap = malloc(sizeof(Bitmap_t));

    bitmap->width = width;
    bitmap->height = height;

    Color_t *colors = v_allocMany(width * height);

    bitmap->pixels = colors;

    return bitmap;
}

void b_done(Bitmap_t *toFree)
{
    free(toFree->pixels);
    free(toFree);
}

void b_raymarchIntoBitmap(Scene_t* scene, Camera_t* cam, Bitmap_t* bitmap) {
    float cx, cy;
    unsigned short bx, by;
    Ray_t* ray;

    for (by = 0; by < bitmap->height; by++) {
        for (bx = 0; bx < bitmap->width; bx++) {
            // Initialize camera coordinates.
            cx = (float)(bx) - bitmap->width / 2;
            cx /= bitmap->width / 2;

            cy = (float)(by) - bitmap->height / 2;
            cy /= bitmap->height / 2;

            // Make a ray.
            ray = c_radiate(cam, cx, cy);

            // Raymarch.
            m_fullRayMarch(scene, ray, b_at(bitmap, bx, by));
        }
    }
}