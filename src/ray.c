#include <stdlib.h>

#include "commons.h"
#include "vector3.h"



Ray_t* r_emptyNew() {
    Ray_t* res = malloc(sizeof(Ray_t));

    res->closest = 0;
    res->from = 0;
    res->currDist = 0;
    res->origin = v_new();
    res->toward = v_new();

    res->toward->x = 1;

    return res;
}


void r_advance(Ray_t* ray, float dist) {
    ray->currDist += dist;
}


Vec3_t* r_currentPos(Ray_t* ray) {
    Vec3_t* offset = v_scale(ray->toward, ray->currDist);
    Vec3_t* res = v_add(ray->origin, offset);
    v_done(offset);

    return res;
}


Ray_t* r_fromTo(Vec3_t* begin, Vec3_t* end) {
    Ray_t* res = r_emptyNew();

    res->origin = v_duplicate(begin);
    res->toward = v_sub(end, begin);
    v_normalizeAt(res->toward);

    return res;
}

Ray_t* r_fromToward(Vec3_t* begin, Vec3_t* toward) {
    Ray_t* res = r_emptyNew();

    res->origin = v_duplicate(begin);
    res->toward = toward;
    v_normalizeAt(res->toward);

    return res;
}


void r_done(Ray_t* ray) {
    free(ray->origin);
    free(ray->toward);
    free(ray);
}


float r_sceneDistance(Ray_t* ray, Scene_t* scene) {
    SceneObject_t* obj = 0;
    float dist = 0, minDist = 0;
    unsigned char distFound = 0;
    Vec3_t* pos = r_currentPos(ray);
    
    ray->closest = 0;

    for (obj = scene->objects; obj; obj = obj->next) {
        dist = (*obj->distanceFunc)(obj, pos);

        if ((dist < minDist) || !distFound) {
            minDist = dist;
            distFound = 1;
            ray->closest = obj;
        }
    }

    v_done(pos);
    return minDist;
}