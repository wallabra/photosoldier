#include <math.h>
#include <stdlib.h>

#include "commons.h"
#include "vector3.h"
#include "ray.h"



Ray_t* c_radiate(Camera_t* cam, float screenX, float screenY) {
    Ray_t* res = r_emptyNew();

    res->origin = v_duplicate(&(cam->origin));

    float slope = tanf(cam->fov / 2);
    Vec3_t* dir = v_new();

    dir->x = 1;
    dir->z = screenX * slope;
    dir->y = screenY * slope;

    v_rotate(dir, &(cam->toward));
    v_normalizeAt(dir);

    res->toward = dir;
    res->from = cam;

    return res;
}