#include <stdio.h>

#include "commons.h"



void printColor(Color_t* color) {
    printf(
        "  <%i, %i, %i>\n",

        (int) (color->x * 255),
        (int) (color->y * 255),
        (int) (color->z * 255)
    );
}