#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "commons.h"
#include "vector3.h"



Vec3_t vx = {1, 0, 0};
Vec3_t vy = {0, 1, 0};
Vec3_t vz = {0, 0, 1};


void v_zero(Vec3_t* ptr) {
    ptr->x = 0;
    ptr->y = 0;
    ptr->z = 0;
}

Vec3_t* v_new() {
    Vec3_t* res = malloc(sizeof(Vec3_t));

    v_zero(res);

    return res;
}

Vec3_t* v_allocMany(size_t howMany) {
    Vec3_t* res = malloc(sizeof(Vec3_t) * howMany);
    for (size_t i = 0; i < howMany; i++) v_zero(res + i);

    return res;
}

Vec3_t* v_duplicate(Vec3_t* from) {
    Vec3_t* res = malloc(sizeof(Vec3_t));

    res->x = from->x;
    res->y = from->y;
    res->z = from->z;

    return res;
}

void v_done(Vec3_t* which) {
    free(which);
}


Vec3_t* v_add(Vec3_t* vec1, Vec3_t* vec2) {
    Vec3_t* res = v_new();

    res->x = vec1->x + vec2->x;
    res->y = vec1->y + vec2->y;
    res->z = vec1->z + vec2->z;

    return res;
}

Vec3_t* v_sub(Vec3_t* vec1, Vec3_t* vec2) {
    Vec3_t* res = v_new();

    res->x = vec1->x - vec2->x;
    res->y = vec1->y - vec2->y;
    res->z = vec1->z - vec2->z;

    return res;
}

Vec3_t* v_addScaled(Vec3_t* vec1, Vec3_t* vec2, float weight) {
    Vec3_t* res = v_new();

    res->x = vec1->x + vec2->x * weight;
    res->y = vec1->y + vec2->y * weight;
    res->z = vec1->z + vec2->z * weight;

    return res;
}

Vec3_t* v_subScaled(Vec3_t* vec1, Vec3_t* vec2, float weight) {
    Vec3_t* res = v_new();

    res->x = vec1->x - vec2->x * weight;
    res->y = vec1->y - vec2->y * weight;
    res->z = vec1->z - vec2->z * weight;

    return res;
}

void v_neg(Vec3_t* vec) {
    vec->x *= -1;
    vec->y *= -1;
    vec->z *= -1;
}


Vec3_t* v_scale(Vec3_t* vec, float scale) {
    Vec3_t* res = v_new();

    res->x = vec->x * scale;
    res->y = vec->y * scale;
    res->z = vec->z * scale;

    return res;
}


Vec3_t* v_multiply(Vec3_t* vec1, Vec3_t* vec2) {
    Vec3_t* res = v_new();

    res->x = vec1->x * vec2->x;
    res->y = vec1->y * vec2->y;
    res->z = vec1->z * vec2->z;

    return res;
}


Vec3_t* v_divide(Vec3_t* vec, float by) {
    Vec3_t* res = v_new();

    res->x = vec->x / by;
    res->y = vec->y / by;
    res->z = vec->z / by;

    return res;
}


Vec3_t* v_normalize(Vec3_t* vec) {
    Vec3_t* res = v_duplicate(vec);
    v_normalizeAt(res);

    return res;
}


void v_addTo(Vec3_t* vec1, Vec3_t* vec2) {
    vec1->x += vec2->x;
    vec1->y += vec2->y;
    vec1->z += vec2->z;
}

void v_subTo(Vec3_t* vec1, Vec3_t* vec2) {
    vec1->x -= vec2->x;
    vec1->y -= vec2->y;
    vec1->z -= vec2->z;
}

void v_addToScaled(Vec3_t* vec1, Vec3_t* vec2, float weight) {
    vec1->x += vec2->x * weight;
    vec1->y += vec2->y * weight;
    vec1->z += vec2->z * weight;
}

void v_subToScaled(Vec3_t* vec1, Vec3_t* vec2, float weight) {
    vec1->x -= vec2->x * weight;
    vec1->y -= vec2->y * weight;
    vec1->z -= vec2->z * weight;
}

void v_scaleTo(Vec3_t* vec, float scale) {
    vec->x *= scale;
    vec->y *= scale;
    vec->z *= scale;
}

void v_multiplyTo(Vec3_t* vec1, Vec3_t* vec2) {
    vec1->x *= vec2->x;
    vec1->y *= vec2->y;
    vec1->z *= vec2->z;
}

void v_divideTo(Vec3_t* vec, float by) {
    vec->x /= by;
    vec->y /= by;
    vec->z /= by;
}

void v_normalizeAt(Vec3_t* vec) {
    float len = vec->x * vec->x + vec->y * vec->y + vec->z * vec->z;
    float half_len = len * 0.5F;

    long evilCarmackMagic = 0x5F3759DF - ((*(long *) &len) >> 1); // explanation: ask John Carmack about his sorcery, not me

    float scale = *(float*)(&evilCarmackMagic);
    v_scaleTo(vec, scale * (1.5F - (half_len * scale * scale))); // explanation: 1st iteration of the Newtom method
}


void v_rotate(Vec3_t* vec, Rot_t* rot) {
    float x2, y2, z2;

    // 1. Roll (x)
    y2 = vec->y * cosf(rot->r) - vec->z * sinf(rot->r);
    z2 = vec->y * sinf(rot->r) + vec->z * cosf(rot->r);

    vec->y = y2;
    vec->z = z2;

    // 2. Yaw (y)
    x2 = vec->x * cosf(rot->y) - vec->z * sinf(rot->y);
    z2 = vec->x * sinf(rot->y) + vec->z * cosf(rot->y);

    vec->x = x2;
    vec->z = z2;

    // 3. Pitch (z)
    x2 = vec->x * cosf(rot->p) - vec->y * sinf(rot->p);
    y2 = vec->x * sinf(rot->p) + vec->y * cosf(rot->p);

    vec->x = x2;
    vec->y = y2;
}

float v_size(Vec3_t* vec) {
    return sqrtf(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
}

float v_dot(Vec3_t* vec1, Vec3_t* vec2) {
    return vec1->x * vec2->x + vec1->y * vec2->y + vec1->z * vec2->z;
}

float v_sqSize(Vec3_t* vec) {
    return vec->x * vec->x + vec->y * vec->y + vec->z * vec->z;
}